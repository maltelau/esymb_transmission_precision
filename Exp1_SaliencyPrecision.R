
source("/work/797605/renv-load.R")
pacman::p_load(tidyverse, brms, bayesplot, purrr, furrr, progressr)

plan(multisession, workers = 16)
iter <- 4e3

nlist <- c(60, 180, 300) ## what sets of n to test
n_sim <- 100              ## how many repetitions

## NoTES
## Re-st@ructure so that the point is getting a ER of 10

####################
#### function definitions
sim_data <- function(nsubjects) {

    ## stim generation
    nchain <- 12
    nseeds <- 4
    nchains <- nchain * 3 # 12 chains per condition, 36 total
    ngenerations <- 3 # which stand for 1 5 and 9
    nstimuli <- nchains * nseeds * ngenerations

    ## forward model

                                        # Generate stimuli, so that each chain has 4 seeds, and each condition 12 chains
    stimuli <- expand_grid(
        seed = seq(nseeds),
        chain = seq(nchains),
        generation = seq(ngenerations)) %>%
        mutate(
            stimulus = seq(nseeds * nchains * ngenerations),
            condition = round((chain - 6) / nchain, 0) + 1,
                                        # in some follow up experiments we sample 2 chains, this is the pre-made sampling
            chainblock = ifelse((chain %% nchain) %in% c(1, 2), 1, 
                         ifelse((chain %% nchain) %in% c(3, 4), 2, 
                         ifelse((chain %% nchain) %in% c(5, 6), 3,
                         ifelse((chain %% nchain) %in% c(7, 8), 4,
                         ifelse((chain %% nchain) %in% c(9, 0), 5, 
                         ifelse((chain %% nchain) %in% c(10, 11), 6, NA)))))),
            stimulusblock = ifelse(seed < 3, 1, 2),
            block = ifelse(stimulusblock == 1, chainblock, chainblock + 6)
        )

                                        # Now we generated the stimuli with their meta data. 
                                        # N.B. stimuli at this stage is just a number with a chain, a block, a condition, etc.

                                        # This generates 48 stimuli per generation 1 5 and 9 per condition: 432 stimuli

    stimuli <- stimuli %>%
        mutate(block = stimulusblock)

                                        # Now we have that each participant gets 36 stimuli: 
                                        # per each condition 2 chains, per each chain 3 generations, 
                                        # per each generation 2 stimuli. Easiest to make 10 blocks.

    participants <- tibble(
        id = seq(nsubjects)## ,
        ## block = rep(1:2, nsubjects/2)
    )


    experiment <- merge(participants, stimuli) %>% mutate(chainblock = NULL, stimulusblock = NULL)

                                        # Setting up parameter values.
                                        # Some are based on the previous experiment (e.g. intercepts)
                                        # Some are based on minimal effect size of interest (e.g. slopes)
    Intercept <- -0.14 # from the CogSci Manuscript
    Intercept_sub <- 0.4 # 0.36 in the CogSci
    Intercept_seed <- 0.08 # not in the CogSci manuscript
    Intercept_chain <- 0.03 # from CS
    Intercept_stimulus <- 0.04 # from CS (where it was 0.11, but some goes to seed)

    Slope <- -0.01 # from CS
    Slope_sub <- 0.01 # from CS
    Slope_seed <- 0.02 # 
    Slope_chain <- 0.02 # from CS

    Sigma <- 0.7 # from CS

    Ndt <- 0.4 # exponentiated from the CS
    Ndt_sub <- 0.43 # from the CS

    ## Salience
    ## Follows a shifted lognormal, where 
    ## mu is conditioned on intercept (varying by participant, seed, chain and stimulus),
    ## and slope (varying by participant, seed and chain); 
    ## sigma is fixed and 
    ## ndt is fixed

    Variance_p <- tibble(id = seq(nsubjects),
                         Intercept = rnorm(nsubjects, 0, Intercept_sub),
                         Slope = rnorm(nsubjects, 0, Slope_sub)#,ndt = rnorm(nsubjects, 0, Ndt_sub)
                         )

    Variance_c <- tibble(chain = seq(nchains),
                         Intercept = rnorm(nchains, 0, Intercept_chain),
                         Slope = rnorm(nchains, 0, Slope_chain))

    Variance_seed <- tibble(seed = seq(nseeds),
                            Intercept = rnorm(nseeds, 0, Intercept_seed),
                            Slope = rnorm(nseeds, 0, Slope_seed))

    Variance_s <- tibble(stimulus = seq(nstimuli), Intercept = rnorm(nstimuli, 0, Intercept_stimulus))

    experiment$rt <- NA
    for (i in seq_len(nrow(experiment))) {
        experiment$rt[i] <- brms::rshifted_lnorm(1,
                                                 meanlog = (Intercept +
                                                            Variance_p$Intercept[Variance_p$id == experiment$id[i]] +
                                                            Variance_c$Intercept[Variance_c$chain == experiment$chain[i]] +
                                                            Variance_seed$Intercept[Variance_seed$seed == experiment$seed[i]] +
                                                            Variance_s$Intercept[Variance_s$stimulus == experiment$stimulus[i]]) +
                                                     (ifelse(experiment$condition[i] != 1, (Slope +
                                                                                            Variance_p$Slope[Variance_p$id == experiment$id[i]] +
                                                                                            Variance_seed$Slope[Variance_seed$seed == experiment$seed[i]] +
                                                                                            Variance_c$Slope[Variance_c$chain == experiment$chain[i]]) *
                                                                                           experiment$generation[i], 0)),
                                                 sdlog = Sigma,
                                                 shift = Ndt)
    }

    ## sanity check to make sure all rt >~ 0.2
    experiment$rt <- ifelse(experiment$rt < 0.2, rnorm(nrow(experiment), 0.2, 0.03), experiment$rt)
    experiment$condition <- as.factor(experiment$condition)

    return(experiment)
}

extract_estimates <- function(model) {
    model %>% 
        as_draws_df() %>% 
        mutate(SlopeEffect = `b_condition3:generation`,
               SlopeNoEffect = `b_condition1:generation`,
               ContrastEffect = `b_condition3:generation` - `b_condition1:generation`,
               ContrastNoEffect = `b_condition3:generation` - `b_condition2:generation`,
               Prediction_condition1_generation1 = `b_ndt_Intercept` + `b_condition1` + 1 * `b_condition1:generation`,
               Prediction_condition1_generation2 = `b_ndt_Intercept` + `b_condition1` + 2 * `b_condition1:generation`,
               Prediction_condition1_generation3 = `b_ndt_Intercept` + `b_condition1` + 3 * `b_condition1:generation`,
               Prediction_condition2_generation1 = `b_ndt_Intercept` + `b_condition2` + 1 * `b_condition2:generation`,
               Prediction_condition2_generation2 = `b_ndt_Intercept` + `b_condition2` + 2 * `b_condition2:generation`,
               Prediction_condition2_generation3 = `b_ndt_Intercept` + `b_condition2` + 3 * `b_condition2:generation`,
               Prediction_condition3_generation1 = `b_ndt_Intercept` + `b_condition3` + 1 * `b_condition3:generation`,
               Prediction_condition3_generation2 = `b_ndt_Intercept` + `b_condition3` + 2 * `b_condition3:generation`,
               Prediction_condition3_generation3 = `b_ndt_Intercept` + `b_condition3` + 3 * `b_condition3:generation`
               ) %>%
        summarize(
            ## effects
            SlopeEffectMean = mean(SlopeEffect),
            SlopeEffectSE = sd(SlopeEffect),
            SlopeEffectLowCI = quantile(SlopeEffect, 0.025),
            SlopeEffectHighCI = quantile(SlopeEffect, 0.975),
            SlopeEffectWidth = SlopeEffectHighCI - SlopeEffectLowCI,
            SlopeER = sum(SlopeEffect > 0) / sum(SlopeEffect <= 0),
            SlopeCred = sum(SlopeEffect > 0) / n(),
            SlopeERNull = hypothesis(model, "condition3:generation = 0")$hypothesis$`Evid.Ratio`,
            SlopeNoEffectMean = mean(SlopeNoEffect),
            SlopeNoEffectSE = sd(SlopeNoEffect),
            SlopeNoEffectLowCI = quantile(SlopeNoEffect, 0.025),
            SlopeNoEffectHighCI = quantile(SlopeNoEffect, 0.975),
            SlopeNoEffectWidth = SlopeNoEffectHighCI - SlopeNoEffectLowCI,
            SlopeNoER = sum(SlopeNoEffect > 0) / sum(SlopeNoEffect <= 0),
            SlopeNoCred = sum(SlopeNoEffect > 0) / n(),
            SlopeNoERNull = hypothesis(model, "condition1:generation = 0")$hypothesis$`Evid.Ratio`,
            ContrastEffectMean = mean(ContrastEffect),
            ContrastEffectSE = sd(ContrastEffect),
            ContrastEffectLowCI = quantile(ContrastEffect, 0.025),
            ContrastEffectHighCI = quantile(ContrastEffect, 0.975),
            ContrastEffectWidth = ContrastEffectHighCI - ContrastEffectLowCI,
            ContrastER = sum(ContrastEffect > 0) / sum(ContrastEffect <= 0),
            ContrastCred = sum(ContrastEffect > 0) / n(),
            ContrastERNull = hypothesis(model, "condition3:generation - condition1:generation = 0")$hypothesis$`Evid.Ratio`,
            ContrastNoEffectMean = mean(ContrastNoEffect),
            ContrastNoEffectSE = sd(ContrastNoEffect),
            ContrastNoEffectLowCI = quantile(ContrastNoEffect, 0.025),
            ContrastNoEffectHighCI = quantile(ContrastNoEffect, 0.975),
            ContrastNoEffectWidth = ContrastNoEffectHighCI - ContrastNoEffectLowCI,
            ContrastNoER = sum(ContrastNoEffect > 0) / sum(ContrastNoEffect <= 0),
            ContrastNoCred = sum(ContrastNoEffect > 0) / n(),
            ContrastNoERNull = hypothesis(model, "condition3:generation - condition2:generation = 0")$hypothesis$`Evid.Ratio`,
            ## ndt
            NDTMean = mean(b_ndt_Intercept),
            NDTSE = sd(b_ndt_Intercept),
            NDTLowCI = quantile(b_ndt_Intercept, 0.025),
            NDTHighCI = quantile(b_ndt_Intercept, 0.975),
            NDTWidth = NDTHighCI - NDTLowCI,
            ## predictions
            Prediction_condition1_generation1_Mean = mean(Prediction_condition1_generation1),
            Prediction_condition1_generation1_LowCI = quantile(Prediction_condition1_generation1, 0.025),
            Prediction_condition1_generation1_HighCI = quantile(Prediction_condition1_generation1, 0.975),
            Prediction_condition1_generation2_Mean = mean(Prediction_condition1_generation2),
            Prediction_condition1_generation2_LowCI = quantile(Prediction_condition1_generation2, 0.025),
            Prediction_condition1_generation2_HighCI = quantile(Prediction_condition1_generation2, 0.975),
            Prediction_condition1_generation3_Mean = mean(Prediction_condition1_generation3),
            Prediction_condition1_generation3_LowCI = quantile(Prediction_condition1_generation3, 0.025),
            Prediction_condition1_generation3_HighCI = quantile(Prediction_condition1_generation3, 0.975),
            Prediction_condition2_generation1_Mean = mean(Prediction_condition2_generation1),
            Prediction_condition2_generation1_LowCI = quantile(Prediction_condition2_generation1, 0.025),
            Prediction_condition2_generation1_HighCI = quantile(Prediction_condition2_generation1, 0.975),
            Prediction_condition2_generation2_Mean = mean(Prediction_condition2_generation2),
            Prediction_condition2_generation2_LowCI = quantile(Prediction_condition2_generation2, 0.025),
            Prediction_condition2_generation2_HighCI = quantile(Prediction_condition2_generation2, 0.975),
            Prediction_condition2_generation3_Mean = mean(Prediction_condition2_generation3),
            Prediction_condition2_generation3_LowCI = quantile(Prediction_condition2_generation3, 0.025),
            Prediction_condition2_generation3_HighCI = quantile(Prediction_condition2_generation3, 0.975),
            Prediction_condition3_generation1_Mean = mean(Prediction_condition3_generation1),
            Prediction_condition3_generation1_LowCI = quantile(Prediction_condition3_generation1, 0.025),
            Prediction_condition3_generation1_HighCI = quantile(Prediction_condition3_generation1, 0.975),
            Prediction_condition3_generation2_Mean = mean(Prediction_condition3_generation2),
            Prediction_condition3_generation2_LowCI = quantile(Prediction_condition3_generation2, 0.025),
            Prediction_condition3_generation2_HighCI = quantile(Prediction_condition3_generation2, 0.975),
            Prediction_condition3_generation3_Mean = mean(Prediction_condition3_generation3),
            Prediction_condition3_generation3_LowCI = quantile(Prediction_condition3_generation3, 0.025),
            Prediction_condition3_generation3_HighCI = quantile(Prediction_condition3_generation3, 0.975)
        ) %>%
        mutate(rhat = mean(brms::rhat(model)),
               divergences = sum(brms::nuts_params(model, pars = "divergent__")$Value),
               elapsed_time = sum(rstan::get_elapsed_time(model$fit)))
    }


sim_d_and_fit <- function(sample, n, seed, progress = NULL) {
    set.seed(seed)
    ## print(paste0("n: ", n, " seed: ", seed))
    source("/work/797605/renv-load.R") # load renv and cmdstanr



    ## sim data
    experiment <- sim_data(n)

    ## fit model
    m1a <- update(RT_m1,
                  newdata = experiment,
                  chains = 1,
                  init = init_list,
                  cores = 1,
                  iter = iter,
                  stan_model_args = list(stanc_options = list("O1")),
                                        #threads = threading(2),
                  control = list(
                      adapt_delta = 0.90,
                      max_treedepth = 20
                  ))
    
    ## extract estimates
    estimate <- extract_estimates(m1a)
    write_csv(estimate, str_c("sim_saliency_full_", n, "_", sample, ".csv"))
    progress(message = paste0("completed i: ", sample, " n: ", n, " seed: ", seed),
             amount = n, class = "sticky")
    if (estimate$rhat[[1]] > 1.01) {
        write_rds(m1a, str_c("model_", n, "_", sample, ".RDS"))
        }
    return(estimate)
}



#########################
#### compile the stan model

## Model (linear)
RT_f1 <- bf(rt ~ 0 + condition + condition:generation +
                (0 + condition + condition:generation | id) +
                (0 + condition + condition:generation | chain) +
                (0 + condition + condition:generation | seed) +
                (1 | gr(stimulus, by = condition)),
            ## sigma ~ 1,
            ndt ~ 1) #+ (1 | p | id)

## get_prior(RT_f1 , experiment, family = shifted_lognormal())

## set priors
RT_p1 <- c(
    prior(normal(0, 0.2), class = b),
    prior(normal(0, 0.3), class = sd),
    prior(normal(0, 0.5), class = sd, group = id),
    prior(normal(0, 0.5), class = sigma),
    prior(normal(-1, 0.7), class = Intercept, dpar = ndt),
    ## prior(normal(0, 0.7), class = sd, dpar = ndt),
    ## prior(normal(0, 0.7), class = sd, group = id, dpar = ndt),
    prior(lkj(3), class = cor))

## initial values
init_list <- list(list(Intercept_ndt = -1, sigma = .1, b = c(-1, -1, -1, 0, 0, 0),
                       sd_1 = rep(.1, 6), sd_2 = rep(.1, 6), sd_3 = rep(.1, 6),
                       sd_4 = matrix(rep(.1, 3), nrow = 1)))

## run 1 sim for testing
experiment <- sim_data(nsubjects = 6)

## posterior
RT_m1 <- brm(
    RT_f1,
    experiment,
    family = shifted_lognormal(),
    prior = RT_p1,
    sample_prior = TRUE,
    chains = 1,
    init = init_list,
    cores = 1,
    iter = 10,
    backend = "cmdstanr",
    stan_model_args = list(stanc_options = list("O1")),
    threads = threading(4),
    control = list(
        adapt_delta = 0.9,
        max_treedepth = 15
    ))


## mcmc_pairs(RT_m1, regex_pars = "b_condition")

## simdata <- extract_estimates(RT_m1) %>%
##     bind_rows(simdata) %>%
##     mutate(time = now(), n = 30)

#### run simulations

simulations <- expand_grid(
    sample = 1:n_sim,
    n = nlist) %>%
    mutate(seed = 1000 * n + sample)


## dont rerun simlations already done
already_done <- list.files(pattern = "sim_saliency.*.csv") %>%
    str_match("sim_saliency_full_(\\d+)_(\\d+).csv") %>%
    as_tibble() %>%
    select(n = V2, sample = V3) %>%
    map_df(as.integer)

simulations <- anti_join(simulations, already_done, by = join_by(sample, n))


progressr::with_progress({
    progress <- progressr::progressor(steps = sum(simulations$n))
    progress(amount = 0)
    result <- future_pmap(simulations, sim_d_and_fit, progress = progress)
})

## ## THEN SETUP A LOOP INCREASING N AND ASSESS POWER
## d_sim <- NULL
## for (n in c(60, 120, 180)) { #
##     print(paste0("Starting simulation for n=", n))
##     temp <-  tibble(seed = 1000:(1000 + n_sim - 1)) %>% 
##         mutate(b1 = future_pmap(seed, sim_d_and_fit, n, .progress = TRUE)) %>% 
##         unnest(b1) %>% mutate(sample = n)
##     if (exists("d_sim")) {d_sim <- rbind(d_sim, temp)} else {d_sim <- temp}
##     write_csv(d_sim, "sim_saliency_data.csv")
## }
## ate(sample = n)
## if (exists("d_sim")) {d_sim <- rbind(d_sim, temp)} else {d_sim <- temp}
## write_csv(d_sim, "sim_saliency_data.csv")
## }
