

data{
  int N;           // amount of data points
  array[N] int y;  // outcome
  
  int N_chains;        
  array[N] int chain_1;  // which stimulus 1 in each trials
  array[N] int chain_2;  // which stimulus 2 in each trials

  int N_seeds;
  array[N] int seed_1;
  array[N] int seed_2;

  int N_id;
  array[N] int id;
  
  int N_beta;               // number of main effects
  matrix[N, N_beta] X1;     // data for main effects for stim 1
  matrix[N, N_beta] X2;     // data for main effects for stim 2
  
  matrix[N, N_beta-1] X1_noint;     // data for main effects for stim 1, no intercept
  matrix[N, N_beta-1] X2_noint;     // data for main effects for stim 2, no intercept

}

parameters {
  vector[N_beta-1] beta; // main effects, no intercept
  
  /* matrix[N_chains, N_beta] ranef_1; // random effects for chain */
  /* matrix[N_seeds, N_beta] ranef_2; // random effects for seed */
  /* matrix[N_id, N_beta-1] ranef_3; // random effects for id, no intercept */
  array[N_chains] vector[N_beta] ranef_1; // random effects for chain
  array[N_seeds] vector[N_beta] ranef_2; // random effects for seed
  array[N_id] vector[N_beta-1] ranef_3; // random effects for id, no intercept
  cholesky_factor_corr[N_beta] Lcorr_1; 
  cholesky_factor_corr[N_beta] Lcorr_2; 
  /* cholesky_factor_corr[N_beta-1] Lcorr_3;  */
  vector<lower=0>[N_beta] sigma_1;
  vector<lower=0>[N_beta] sigma_2;
  vector<lower=0>[N_beta-1] sigma_3;
}

transformed parameters{
  corr_matrix[N_beta] R_1; 
  corr_matrix[N_beta] R_2; 
  /* corr_matrix[N_beta-1] R_3;  */
  cov_matrix[N_beta] Sigma_1;
  cov_matrix[N_beta] Sigma_2;
  /* cov_matrix[N_beta-1] Sigma_3; */
  R_1 = multiply_lower_tri_self_transpose(Lcorr_1);
  R_2 = multiply_lower_tri_self_transpose(Lcorr_2);
  /* R_3 = multiply_lower_tri_self_transpose(Lcorr_3); */
  Sigma_1 = quad_form_diag(R_1, sigma_1); // quad_form_diag: diag_matrix(sig) * R * diag_matrix(sig)
  Sigma_2 = quad_form_diag(R_2, sigma_2); // quad_form_diag: diag_matrix(sig) * R * diag_matrix(sig)
  /* Sigma_3 = quad_form_diag(R_3, sigma_3); // quad_form_diag: diag_matrix(sig) * R * diag_matrix(sig) */
}

model {
  vector[N] mu_1;
  vector[N] mu_2;
  
  beta    ~ normal(0, 5);
  
  sigma_1 ~ normal(0, 5);
  sigma_2 ~ normal(0, 5);
  sigma_3 ~ normal(0, 5);

  
  ranef_1 ~ multi_normal(rep_vector(0, N_beta), Sigma_1);
  ranef_2 ~ multi_normal(rep_vector(0, N_beta), Sigma_2);
  ranef_3[,1] ~ normal(0, sigma_3[1]);

  
  /* for (k in 1:N_chains) { */
  /* 	ranef_1[,k] ~ multi_normal(rep_vector(0, N_beta), Sigma_1); */
  /* } */
  /* for (k in 1:N_seeds) { */
  /* 	ranef_2[,k] ~ multi_normal(rep_vector(0, N_beta), Sigma_2); */
  /* } */

  
  /* for (k in 1:N_id) { */
  /* 	ranef_3 ~ multi_normal(rep_vector(0, N_beta-1), Sigma_3); */
  /* } */
  Lcorr_1 ~ lkj_corr_cholesky(2.0);
  Lcorr_2 ~ lkj_corr_cholesky(2.0);
  /* Lcorr_3 ~ lkj_corr_cholesky(2.0); */

  /* mu_1 = X1_noint * beta + rows_dot_product(X1_noint, ranef_3[id]) + rows_dot_product(X1, ranef_1[chain_1] + ranef_2[seed_1]); */
  /* mu_2 = X2_noint * beta + rows_dot_product(X2_noint, ranef_3[id]) + rows_dot_product(X2, ranef_1[chain_2] + ranef_2[seed_2]); */
  for (n in 1:N) {
	
	// main effects and participant level random effects
	// only have a slope and no intercept. generation * (b + b[id])
	// chain and seed level random effects have intercept + slope
	// [intercept, generation] * (b[chain] + b[seed]
	
	mu_1[n]  = X1_noint[n] * (beta + ranef_3[id[n]]) +
			   X1[n] * (ranef_1[chain_1[n]] + ranef_2[seed_1[n]]);
	mu_2[n]  = X2_noint[n] * (beta + ranef_3[id[n]]) +
			   X2[n] * (ranef_1[chain_2[n]] + ranef_2[seed_2[n]]);
  }
  
  y     ~ bernoulli_logit(mu_1 - mu_2);  
}
